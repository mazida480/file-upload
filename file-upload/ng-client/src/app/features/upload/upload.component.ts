import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UploadService } from 'src/app/shared/upload.service';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css']
})
export class UploadComponent implements OnInit {

  public form!:FormGroup
  public imagePath = ''
  constructor(private uploadService:UploadService, private fb:FormBuilder){}

  ngOnInit(): void {
    this.form = this.fb.group(
      {
        image:[null, Validators.required]
      }
    )
    
  }

  onImageSelect(e:any){
    const file = (e.target as HTMLInputElement).files

    if(file && file.length > 0){
      this.form.patchValue({image:file[0]})
    }

    this.form.get('image')?.updateValueAndValidity()
    

  }

  onSubmit(){
    const formData = new FormData()
    formData.append('myfile', this.form.get('image')?.value)

    this.uploadService.upload(formData).subscribe((res:any) => {
      console.log(res.file)
    })

  }

  get f(){
    return this.form.controls
  }



}
