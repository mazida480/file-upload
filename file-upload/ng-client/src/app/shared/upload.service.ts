import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, catchError, map, tap, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UploadService {

  // httpOptions = {
  //   headers: new HttpHeaders({ 'Content-Type': 'image/jpeg' })
  // };

  private baseUrl:string = 'http://localhost:3000'

  constructor(private http:HttpClient) { }

  upload(image:any) : Observable<any>{
    return this.http.post<File>(`${this.baseUrl}/upload`, image).pipe(
      tap((yourImage:File) => console.log(`${yourImage} is uploaded`)),

      catchError(this.handleError('upload'))
    )
  }

  private handleError<T>(operation = 'operation', result?:T){
      return(error:any) : Observable<T> => {
        console.log(error)

        return of(result as T)
      }

  }

  test(image:any): Observable<any>{
    return this.http.post<any>(`${this.baseUrl}/upload`, image)
  }
}
