const express = require('express')
const path = require('node:path')
const router = express.Router()
const multer = require('multer')

const myStorage = multer.diskStorage(
    {
        destination : function(req, file, cb){
            return cb(null, './uploads')
        },
        filename : function(req, file, cb){
           return cb(null, `${Date.now()}-${file.originalname}` )

        }
    }
)

const maxFileSize = 5 * 1000 * 1000
// const upload = multer(
//     {
//         storage:myStorage,
//         limits:maxFileSize
//     })

const upload = multer(
    {
        storage:myStorage,
        limits:{fileSize:maxFileSize},
        fileFilter: (req, file, cb) => {
            const fileType = /jpeg|jpg|png/
            const mimeType = fileType.test(file.mimetype)
            const extName = fileType.test(path.extname(file.originalname).toLowerCase())

            if(mimeType && extName){
                return cb(null, true)
            }else{
                 cb(`Only supports the file type: ${fileType}`)
            }

        }
    })

router.post('/upload', upload.single('myfile'), (req, res) => {

    if(req.file){
        const name = req.file.originalname
        res.send({'message':`Your file ${name} is uploaded`, 'file':req.file})
    }else{
        res.send({"message":'Error occured'})
    }
})  

router.get('/', (req, res) => {
    res.send("File upload app is here.")
})

module.exports = router