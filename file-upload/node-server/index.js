const express = require('express')
const app = express()
//Require global module: npm link cors, body-parser
const cors = require('cors') 
const bodyParser = require('body-parser')
const port = 3000
const router = require('./router')

app.use(express.urlencoded({extended:false}))
app.use(cors({origin:'http://localhost:4200'}))

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

app.listen(port, () => {
    console.log(`App is runnint at port ${port}`)
})

app.use("", router)